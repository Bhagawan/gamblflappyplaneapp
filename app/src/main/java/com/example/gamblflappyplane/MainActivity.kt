package com.example.gamblflappyplane

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.gamblflappyplane.databinding.ActivityMainBinding
import com.example.gamblflappyplane.databinding.PopupEndscreenBinding
import com.example.gamblflappyplane.databinding.PopupLostBinding
import com.example.gamblflappyplane.game.FlappyPlaneGameView
import com.example.gamblflappyplane.game.FlappyPlaneViewModel
import com.example.gamblflappyplane.util.Prefs
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var gameViewModel: FlappyPlaneViewModel
    private var coeffPopJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        gameViewModel = ViewModelProvider(this)[FlappyPlaneViewModel::class.java]

        binding.flappyPlaneGameView.setInterface(object: FlappyPlaneGameView.FlappyPlaneInterface {
            override fun onGameEnd() {
                gameViewModel.endRun()
            }

            override fun updatePoints(points: Int) {
                gameViewModel.updateScore(points)
            }
        })

        gameViewModel.updateWallet(Prefs.getWallet(this))

        binding.btnRestart.setOnClickListener {
            gameViewModel.resetGame()
            binding.flappyPlaneGameView.resetGame()
        }
        binding.btnPlay.setOnClickListener { gameViewModel.startRun() }
        binding.btnBetIncrease1.setOnClickListener { gameViewModel.increaseBet(1) }
        binding.btnBetIncrease10.setOnClickListener { gameViewModel.increaseBet(10) }
        binding.btnBetIncrease100.setOnClickListener { gameViewModel.increaseBet(100) }


        lifecycleScope.launch {
            gameViewModel.wallet.collect {
                Prefs.updateWallet(this@MainActivity, it)
                binding.textWallet.text = it.toString()
            }
        }
        lifecycleScope.launch {
            gameViewModel.bet.collect {
                binding.textBet.text = it.toString()
            }
        }

        lifecycleScope.launch {
            gameViewModel.gameRestart.collect {
                if(it) binding.flappyPlaneGameView.restart()
            }
        }

        lifecycleScope.launch {
            gameViewModel.coeff.collect {
                if(it > 0.0f) {
                    binding.textCoeff.text = String.format(getString(R.string.coeff_string), it)
                    binding.textCoeff.visibility = View.VISIBLE
                    coeffPopJob = lifecycleScope.launch {
                        delay(1000)
                        binding.textCoeff.visibility = View.GONE
                    }
                }
            }
        }

        binding.root.post {
            lifecycleScope.launch {
                gameViewModel.gameBroke.collect { if(it) showBrokePopup() }
            }
            lifecycleScope.launch {
                gameViewModel.gameEnd.collect { if(it) showEndRunPopup((gameViewModel.coeff.value * 10).toInt(), gameViewModel.bet.value, gameViewModel.coeff.value) }
            }
        }
        setContentView(binding.root)
    }

    override fun onResume() {
        val s = gameViewModel.getGameState()
        if(s != null) binding.flappyPlaneGameView.setGameState(s)
        super.onResume()
    }

    override fun onPause() {
        gameViewModel.setState(binding.flappyPlaneGameView.getState())
        super.onPause()
    }

    override fun onDestroy() {
        coeffPopJob?.cancel()
        super.onDestroy()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showEndRunPopup(points: Int, bet: Int, coeff: Float) {
        val popupBinding: PopupEndscreenBinding = PopupEndscreenBinding.inflate(layoutInflater, binding.root,false)

        val width = (binding.root.width / 1.3f).toInt()
        val height = if(binding.root.width > binding.root.height) (binding.root.height * 0.7f).toInt() else (binding.root.height * 0.5f).toInt()

        popupBinding.textPopEndPoints.text = points.toString()
        popupBinding.textPopEndBet.text = bet.toString()
        popupBinding.textPopEndCoeff.text = coeff.toString()
        popupBinding.textPopEndWin.text = (bet * coeff).toInt().toString()
        val popupWindow = PopupWindow(popupBinding.root, width, height, true)

        popupBinding.root.setOnClickListener {
            popupWindow.dismiss()
        }
        popupWindow.setOnDismissListener {
            gameViewModel.updateBet()
        }
        popupWindow.setTouchInterceptor { _, _ ->
            popupWindow.dismiss()
            true
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showBrokePopup() {
        val popupBinding: PopupLostBinding = PopupLostBinding.inflate(layoutInflater, binding.root,false)

        val width = (binding.root.width / 1.3f).toInt()
        val height = if(binding.root.width > binding.root.height) (binding.root.height * 0.7f).toInt() else (binding.root.height * 0.5f).toInt()

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)

        popupBinding.root.setOnClickListener {
            popupWindow.dismiss()
        }

        popupWindow.setOnDismissListener {
            gameViewModel.resetGame()
            binding.flappyPlaneGameView.resetGame()
        }
        popupWindow.setTouchInterceptor { _, _ ->
            popupWindow.dismiss()
            true
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }
}