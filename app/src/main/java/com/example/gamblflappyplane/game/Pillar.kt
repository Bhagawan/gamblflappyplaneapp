package com.example.gamblflappyplane.game

data class Pillar(var x : Float, val holeY : Int, var passed : Boolean)
