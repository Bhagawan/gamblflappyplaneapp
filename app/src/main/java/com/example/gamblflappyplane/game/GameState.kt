package com.example.gamblflappyplane.game

data class GameState(val planeY: Float, val planeAngle: Float, val currentImpulse : Float, val record: Int, val pillars: List<Pillar>, val gameState: Int, val pillarSpeed: Float)