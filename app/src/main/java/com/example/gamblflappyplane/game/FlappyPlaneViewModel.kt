package com.example.gamblflappyplane.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class FlappyPlaneViewModel: ViewModel() {
    private val _gameRestart = MutableSharedFlow<Boolean>(1)
    val gameRestart: SharedFlow<Boolean> = _gameRestart

    private val _gameEnd = MutableSharedFlow<Boolean>(1)
    val gameEnd: SharedFlow<Boolean> = _gameEnd

    private val _gameBroke = MutableSharedFlow<Boolean>(1)
    val gameBroke: SharedFlow<Boolean> = _gameBroke

    private val _wallet = MutableStateFlow(0)
    val wallet: StateFlow<Int> = _wallet

    private val _coeff = MutableStateFlow(0.0f)
    val coeff: StateFlow<Float> = _coeff

    private val _bet = MutableStateFlow(100)
    val bet: StateFlow<Int> = _bet


    fun updateWallet(amount: Int) {
        if(amount < bet.value && gameBroke.replayCache.lastOrNull() != true) resetGame()
        else viewModelScope.launch {
            _wallet.emit(amount)
        }
    }

    fun increaseBet(amount: Int) {
        if(gameRestart.replayCache.lastOrNull() != true) viewModelScope.launch {
            _bet.emit((bet.value + amount).coerceAtMost(wallet.value))
        }
    }

    fun startRun() {
        if(gameRestart.replayCache.lastOrNull() != true) {
            viewModelScope.launch {
                _wallet.emit(wallet.value - bet.value)
            }
            viewModelScope.launch {
                _gameRestart.emit(true)
            }
            viewModelScope.launch {
                _gameEnd.emit(false)
            }
            viewModelScope.launch {
                _gameBroke.emit(false)
            }
        }
    }

    fun updateScore(score: Int) {
        viewModelScope.launch {
            _coeff.emit(score * 0.1f)
        }
    }

    fun endRun() {
        viewModelScope.launch {
            _wallet.emit(wallet.value + (bet.value * coeff.value).toInt())
        }
        viewModelScope.launch {
            _gameRestart.emit(false)
        }
        viewModelScope.launch {
            _gameEnd.emit(true)
        }
    }

    fun resetGame() {
        viewModelScope.launch {
            _wallet.emit(1000)
        }
        viewModelScope.launch {
            _gameBroke.emit(false)
        }
        viewModelScope.launch {
            _bet.emit(100)
        }
        viewModelScope.launch {
            _coeff.emit(0.0f)
        }
        viewModelScope.launch {
            _gameRestart.emit(false)
        }
    }

    fun updateBet() {
        viewModelScope.launch {
            _bet.emit((100).coerceAtMost(wallet.value))
        }
        if(wallet.value == 0) {
            viewModelScope.launch {
                _gameBroke.emit(true)
            }
        }
    }


    private var gameState: GameState? = null

    fun setState(state: GameState) {
        gameState = state
    }

    fun getGameState(): GameState? = gameState


}