package com.example.gamblflappyplane.util

import android.content.Context

class Prefs {

    companion object {
        fun getWallet(context: Context) : Int = context.getSharedPreferences("FlappyPlaneGame", Context.MODE_PRIVATE).getInt("wallet", 1000)

        fun updateWallet(context: Context, amount: Int) {
            context.getSharedPreferences("FlappyPlaneGame", Context.MODE_PRIVATE)
                .edit()
                .putInt("wallet", amount)
                .apply()
        }
    }
}