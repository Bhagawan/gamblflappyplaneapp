package com.example.gamblflappyplane.util

import androidx.annotation.Keep

@Keep
data class GamblFlappySplashResponse(val url : String)